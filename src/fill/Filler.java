package fill;

import model.Point;

public interface Filler {

    void fill(Point p);

}
