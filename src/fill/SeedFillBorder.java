package fill;

import model.Point;
import rasterize.Raster;

import java.awt.*;

public class SeedFillBorder implements Filler {

    private Raster raster;
    private int borderColor = Color.yellow.getRGB();
    private int foregroundColor = Color.lightGray.getRGB();

    public SeedFillBorder(Raster raster) {
        this.raster = raster;
    } //Trochu jiná implementace, jen tak pro zábavu a odreágování od scanline

    public void fill(Point p) {
        int x = p.getX();
        int y = p.getY();

        if (
                (x>=0)&&(y>=0)&&(x < raster.getWidth()&&(y< raster.getHeight()))
        ){
            if (raster.getPixel(x,y) != borderColor){ //Algorytmus stejný jako předešlý. Zde akorát kontroluji, jestli nepracuji s pixelem co má barvu ohraničení
                raster.setPixel(x,y,foregroundColor);
                fill(new Point(x-1,y));
                fill(new Point(x+1,y));
                fill(new Point(x,y-1));
                fill(new Point(x,y+1));
            }else{return;}

        }
    }
}
