package fill;

import model.Point;
import rasterize.Raster;

import java.awt.*;

public class PatternSeedFill {

    private Raster raster;
    private int borderColor = Color.yellow.getRGB();
    private int foregroundColor = Color.lightGray.getRGB();

    public PatternSeedFill(Raster raster) {
        this.raster = raster;
    } //Implementace jako border fill

    public void fill(model.Point p) {
        int x = p.getX();
        int y = p.getY();

        if (
                (x>=0)&&(y>=0)&&(x < raster.getWidth()&&(y< raster.getHeight()))
        ){
            if (raster.getPixel(x,y) != borderColor && raster.getPixel(x,y) != foregroundColor && raster.getPixel(x,y) != Color.red.getRGB()){ //Algorytmus stejný jako předešlý. Zde akorát kontroluji, jestli nepracuji s pixelem co má barvu ohraničení
                if (x%2==0){
                    raster.setPixel(x,y,foregroundColor);
                }else{
                    raster.setPixel(x, y, Color.red.getRGB());
                }
                fill(new model.Point(x-1,y));
                fill(new model.Point(x+1,y));
                fill(new model.Point(x,y-1));
                fill(new Point(x,y+1));
            }else{return;}

        }
    }
}
