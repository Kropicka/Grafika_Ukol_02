package controller;

import clip.Clipper;
import fill.*;
import misc.Sorter;
import model.*;
import model.Point;
import model.Polygon;
import rasterize.*;
import view.Panel;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class Controller2D {

    private final Panel panel;
    private final Raster raster;
    private Polygon polygonHolder;

    private boolean triangleMode = false;
    private Triangle triangleHolder;

    private PatternSeedFill seedFillMachine;
    private ScanLine lineFillMachine;
    private PatternScanLine patternFillMachine;

    public Controller2D(Panel panel) {
        this.panel = panel;
        this.raster = panel.getRaster();

        //Práce s polygonem
        polygonHolder = new Polygon(raster);
        triangleHolder = new Triangle(raster);

        //Záplavové vyplňění
        seedFillMachine = new PatternSeedFill(raster);

        //Inicializační metoda
        initListeners();
    }

    private void initListeners() { //Inicializační metoda (Potřebná pro listenery
        panel.addMouseListener(new MouseAdapter() {
            //stlačené tlačítko
            @Override
            public void mousePressed(MouseEvent e) { //Odposlouchávání ztlačení myši
                if (e.getButton() == 1) {
                    if (!triangleMode) { //Přidání bodu do polygonu
                        raster.clear();
                        polygonHolder.addPoint(new Point(e.getX(), e.getY()));
                        polygonHolder.rasterize();
                    } else { //Stavba thaletova trojuhelníku
                        triangleHolder.addPoint(new Point(e.getX(), e.getY()));
                        triangleHolder.rasterizeTriangle();
                    }
                }



                if (e.getButton() == 3 && !triangleMode){ //Ukázka přesunu bodu
                    polygonHolder.findNearest(new Point(e.getX(), e.getY()));
                }


            //Release tlačítka
            }
             @Override
             public void mouseReleased(MouseEvent e){ //Přesun bodu
                 if (e.getButton() == 3 && !triangleMode){
                     polygonHolder.replaceNearest(new Point(e.getX(), e.getY()));
                     raster.clear();
                     polygonHolder.rasterize();
                 }

                 if(e.getButton() == 2){
                     seedFillMachine.fill(new Point(e.getX(),e.getY())); // Pattern seed fil zadání semínka
                 }

            }
        });

        panel.addMouseMotionListener(new MouseAdapter() {

            //Pohnutí myši
            @Override
            public void mouseDragged(MouseEvent e) { //Odposlouchávání pro pohnutí myší (Znázornění vygenerování)
                if (e.getModifiersEx() == 1024 && !triangleMode){ // Znázornění stavby polygonu
                    raster.clear();
                    polygonHolder.showWhere(new Point(e.getX(),e.getY()));
                }
                if (e.getModifiersEx() == 4096 && !triangleMode){ // Přesun bodu
                    raster.clear();
                    polygonHolder.rasterize();
                    polygonHolder.showNearestReplacePoint(new Point(e.getX(), e.getY()));
                }

            }
        });

        panel.addKeyListener(new KeyAdapter() {

            @Override
            public void keyReleased(KeyEvent e) { //Adapter pro odposlouchávání

                if (e.getKeyChar() == 'c') { //tlačítko "C" - Vyčištění plochy
                    System.out.println("----------");
                    System.out.println("Začátek čistícího procesu");
                    raster.clear();
                    polygonHolder.clear();
                    triangleHolder.clear();
                    System.out.println("Čištění plochy kompletní");
                    System.out.println("----------");
                }

                if (e.getKeyChar() == 'v' && !triangleMode) { //tlačítko "V" - Odebrání posledního bodu
                    raster.clear();
                    polygonHolder.removeLast();
                    polygonHolder.rasterize();
                }

                if (e.getKeyChar() == 't') { //tlačítko "T" - Zapnutí trojuhelníkového módu
                    raster.clear();
                    triangleHolder.clear();
                    polygonHolder.clear();
                    if (triangleMode) {
                        triangleMode = false;
                        System.out.println("Trojůhelníkový mod vypnut");
                    } else {
                        triangleMode = true;
                        System.out.println("Trojůhelníkový mod zapnout");
                    }
                }

                if (e.getKeyChar() == 'b' && !triangleMode) { //tlačítko "B" - Scanline aplikace

                    //Deklarace klasické scanline shavrony
                    List<Point> shevrone1 = new ArrayList<>();
                    shevrone1.add(new Point(30, 50));
                    shevrone1.add(new Point(50, 30));
                    shevrone1.add(new Point(70, 50));
                    shevrone1.add(new Point(70, 120));
                    shevrone1.add(new Point(30, 120));

                    //Spuštění


                    patternFillMachine = new PatternScanLine(raster, Color.green, Color.lightGray, shevrone1);
                    patternFillMachine.fill();

                    if(polygonHolder.getPoints().size()>2) {
                        lineFillMachine = new ScanLine(raster, Color.green, Color.lightGray, polygonHolder.getPoints());
                        lineFillMachine.fill();
                    }else{
                        System.out.println("Nedostatek bodů pro vyplnění polygonu");
                    }
                }
            }
        });
    }
}
